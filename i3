# This file has been auto-generated by i3-config-wizard(1)2
# It will not be overwritten, so edit it as you like.
#
# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
#

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8
font pango:System San Francisco Display 10

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Make the focused window not follow the mouse
focus_follows_mouse no

# start a terminal
bindsym $mod+Return exec kitty

# start a browser
bindsym $mod+b exec sh -c "sudo -u ff firefox-bin"

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_solarized
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# Open ranger
bindsym $mod+m exec kitty ranger

# Pulseaudio volume control
bindsym XF86AudioMute exec pactl set-sink-mute 0 toggle
bindsym XF86AudioLowerVolume exec pactl set-sink-mute 0 0 && pactl set-sink-volume 0 -5%
bindsym XF86AudioRaiseVolume exec pactl set-sink-mute 0 0 && pactl set-sink-volume 0 +5%

# Media player keys for vlc
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioPause exec playerctl pause-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+y split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+z focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Switch to next or previous workspace
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

# Move focused workspace to monitor
bindsym $mod+Shift+bracketleft move workspace to output left
bindsym $mod+Shift+bracketright move workspace to output right

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
	# These bindings trigger as soon as you enter the resize mode

	# Pressing left will shrink the window’s width.
	# Pressing right will grow the window’s width.
	# Pressing up will shrink the window’s height.
	# Pressing down will grow the window’s height.
	bindsym h resize shrink width 10 px or 10 ppt
	bindsym j resize grow height 10 px or 10 ppt
	bindsym k resize shrink height 10 px or 10 ppt
	bindsym l resize grow width 10 px or 10 ppt

	# same bindings, but for the arrow keys
	bindsym Left resize shrink width 10 px or 10 ppt
	bindsym Down resize grow height 10 px or 10 ppt
	bindsym Up resize shrink height 10 px or 10 ppt
	bindsym Right resize grow width 10 px or 10 ppt

	# back to normal: Enter or Escape or $mod+r
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# sleep, lock, logout, suspend, shutdown, reboot, etc
set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
mode "$mode_system" {
	bindsym l exec --no-startup-id i3exit lock, mode "default"
	bindsym $mod+l exec --no-startup-id i3exit lock, mode "default"

	bindsym e exec --no-startup-id i3exit logout, mode "default"
	bindsym $mod+e exec --no-startup-id i3exit logout, mode "default"

	bindsym s exec --no-startup-id i3exit suspend, mode "default"
	bindsym $mod+s exec --no-startup-id i3exit suspend, mode "default"

	bindsym h exec --no-startup-id i3exit hibernate, mode "default"
	bindsym $mod+h exec --no-startup-id i3exit hibernate, mode "default"

	bindsym r exec --no-startup-id i3exit reboot, mode "default"
	bindsym $mod+r exec --no-startup-id i3exit reboot, mode "default"

	bindsym x exec prompt "Are you sure you want to shutdown?" "i3exit shutdown", mode "default"
	bindsym $mod+x exec prompt "Are you sure you want to shutdown?" "i3exit shutdown", mode "default"

	# Back to normal: Enter or Escape or $mod+p
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+p mode "default"
}
bindsym $mod+p mode "$mode_system"

# Solarized
set $base03		#002b36
set $base02		#073642
set $base01		#586e75
set $base00		#657b83
set $base0		#839496
set $base1		#93a1a1
set $base2		#eee8d5
set $base3		#fdf6e3
set $yellow		#b58900
set $orange		#cb4b16
set $red		#dc322f
set $magenta	#d33682
set $violet		#6c71c4
set $blue		#268bd2
set $cyan		#2aa198
set $green		#859900

# window colors
#						border		background	text	indicator
client.focused			$green		$green		$base3	$violet
client.unfocused		$base02		$base02		$base1	$base01
client.focused_inactive	$base02		$base02		$base2	$violet
client.urgent			$magenta	$magenta	$base3	$red

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
	status_command i3status
	position top

	colors {
		background $base03

		#					border	background	text
		focused_workspace	$green	$green		$base3	# This workspace
		active_workspace	$blue	$blue		$base3	# Active on other disp
		inactive_workspace	$base02	$base02		$base1	# Any other workspace
	}
}

# i3-gaps configuration
for_window [class="^.*"] border pixel 0
gaps inner 15
#smart_gaps on # Only use gaps when >1 container in workspace

set $mode_gaps Gaps: (0) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
	bindsym o      mode "$mode_gaps_outer"
	bindsym i      mode "$mode_gaps_inner"
	bindsym Return mode "default"
	bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
	bindsym plus  gaps inner current plus 5
	bindsym minus gaps inner current minus 5
	bindsym 0     gaps inner current set 0

	bindsym Shift+plus  gaps inner all plus 5
	bindsym Shift+minus gaps inner all minus 5
	bindsym shift+0     gaps inner all set 0

	bindsym Return mode "default"
	bindsym Escape mode "default"
}

mode "$mode_gaps_outer" {
	bindsym plus  gaps outer current plus 5
	bindsym minus gaps outer current minus 5
	bindsym 0     gaps outer current set 0

	bindsym Shift+plus  gaps outer all plus 5
	bindsym Shift+minus gaps outer all minus 5
	bindsym shift+0     gaps outer all set 0

	bindsym Return mode "default"
	bindsym Escape mode "default"
}

# Project mode for multiple monitors
bindsym $mod+Shift+w exec displayselect
