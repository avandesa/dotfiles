# ~/.zshrc - run before interactive sessions

# General settings
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt nomatch notify
unsetopt autocd beep extendedglob
bindkey -e

function source_if_exists() {
    if [[ -f $1 ]]; then
        source $1
    fi
}

ZSH_CONFIG_DIR="${HOME}/.zsh"

# Initialize antigen
source_if_exists "${ZSH_CONFIG_DIR}/plugins"

# Load aliases
source_if_exists "${ZSH_CONFIG_DIR}/aliases"

# Load directory shortcuts
source_if_exists "${ZSH_CONFIG_DIR}/shortcuts"

# Load keybindings
source_if_exists "${ZSH_CONFIG_DIR}/keybindings"

# Load starship
eval "$(starship init zsh)"

# Load completions
source_if_exists "${ZSH_CONFIG_DIR}/completions"

# Load other settings
source_if_exists "${ZSH_CONFIG_DIR}/misc"

# Load machine-specific settings (in `~/.zsh_local')
source_if_exists "${HOME}/.zsh_local"
