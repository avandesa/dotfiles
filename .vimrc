execute pathogen#infect()

filetype plugin on
filetype plugin indent on

" Colors
syntax enable					" Enables syntax highlighting
set background=dark				" Background theme
colorscheme solarized			" Sets colorscheme to solarized

" Airline settings
let g:airline_theme='solarized'		" Sets the theme for airline
let g:airline_solarized_bg='dark'	" Sets the solarized setting for airline
let g:airline#extensions#tabline#enabled=1 " Show list of buffers
let g:airline#extensions#tabline#fnamemod=':t' " Show just filename

" QuickScope settings
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Spaces and tabs
set tabstop=4		" Number of cols per tab character
set softtabstop=4	" Number of cols per tab key
set shiftwidth=4	" Number of cols per << and >>
set expandtab		" Tabs are spaces
set textwidth=99	" 100 cols per line
set autoindent		" align new line indent w/ previous line
set shiftround		" indent is multiple of `shiftwidth`

" UI Config
set number relativenumber	" Show line numbers
set scrolloff=1000
set showcmd			" Show the last command entered
set cursorline		" Highlights the current line
filetype indent on	" Indent based on different filetypes
set wildmenu		" Visual autocomplete for command menu
set lazyredraw		" Only redraw when needed
set showmatch		" Shows matching parens or braces

" Disable auto-commenting
"autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at bottom and right
set splitbelow splitright

" Shortcutting split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

cmap w!! w !sudo tee > /dev/null %

" Searching
set incsearch	" Search as characters are entered
"set hlsearch	" Highlight matches

" Movement
" Move vertically by visual line
nnoremap j gj
nnoremap k gk

let mapleader=","	" leader is comma
" Clear search results with comma+space
nnoremap <leader><space> :nohlsearch<CR>

" Buffer control.
nnoremap <F2> :bp<CR>
nnoremap <F3> :bd<CR>
nnoremap <F4> :bn<CR>

" Location control
nnoremap <F10> :lnext<CR>
nnoremap <F9>  :lprevious<CR>

nnoremap <F11> :RustFmt<CR>

" jk is escape
inoremap jk <esc>

" Rust configuration

set wildignore+=Cargo.lock " Ignore Cargo.lock in autocomplete

" ALE Configuration
let g:ale_fixers = {
\	'rust': ['rustfmt'],
\   'haskell': ['brittany'],
\}
let g:ale_rust_rustfmt_options = '--edition 2018'
let b:ale_lint_on_save = 1
let b:ale_fix_on_save = 1
